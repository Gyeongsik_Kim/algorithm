#include <iostream>
#include <string>

#define DEBUG

using namespace std;

typedef struct {
	int value;
	int num;
	int lastIndex;
} Item;

Item Items[1000];


void main() {
	int size;
	cout << "SIZE : ";
	cin >> size;

	for (int i = 0; i < size; i++) {
		int value;
		cin >> value;
		Items[i].value = value;
		Items[i].lastIndex = -1;
		Items[i].num = 1;
	}


	#ifdef DEBUG
	cout << endl << "===========" << endl << "DEBUG" << endl << "===========" << endl;
	for (int i = 0; i < size; i++) {
		Item item = Items[i];
		cout << item.value << " " << item.num << " " << item.lastIndex << endl;
	}
	#endif

	for (int i = 0; i < size; i++) {
		if (i == 0)
			continue;
		int num = 0;
		for (int j = 0; j < i; j++) {
			if (Items[i].value > Items[j].value && Items[j].num > num)
				num = Items[j].num;
		}
		Items[i].num = num + 1;
	}

	#ifdef DEBUG
	cout << endl << "===========" << endl << "DEBUG" << endl << "===========" << endl;
	for (int i = 0; i < size; i++) {
		Item item = Items[i];
		cout << item.value << " " << item.num << " " << item.lastIndex << endl;
	}
	#endif

	for (int i = 0; i < size; i++) {
		int last = -1, lastIndex = -1;
		for (int j = 0; j < i; j++) {
			if (Items[j].num >= last && Items[j].value < Items[i].value) {
				last = Items[j].num;
				lastIndex = j;
			}
		}
		if (last == Items[i].num)
			continue;
		else {
			Items[i].lastIndex = lastIndex;
		}
	}
	

	#ifdef DEBUG
		cout << endl << "===========" << endl << "DEBUG" << endl << "===========" << endl;
		for (int i = 0; i < size; i++) {
			Item item = Items[i];
			cout << item.value << " " << item.num << " " << item.lastIndex << endl;
		}
	#endif

	int max = 0;
	for (int i = 0; i < size; i++) {
		if (Items[i].num > Items[max].num)
			max = i;
	}
	
	int index = max;
	max = 0;
	cout << "===========" << endl << "RESULT" << endl << "===========" << endl;
	int arr[100] = { 0, };
	while (index != -1) {
		arr[max] = Items[index].value;
		index = Items[index].lastIndex;
		max += 1;
	}

	while (max != 0) {
		cout << arr[--max] << " ";
	}
	cout << endl;
	system("pause");
}

