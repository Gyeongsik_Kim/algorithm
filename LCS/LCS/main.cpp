#include <iostream>
#include <string>

#define DEBUG 

#define max(X,Y) ((X) > (Y) ? (X) : (Y))  
using namespace std;

int table[1234][1234];

void main() {
	string a, b;
	cin >> a >> b;
	for (size_t i = 1; i <= a.size(); i++) {
		for (size_t j = 1; j <= b.size(); j++) {
			if (a[i - 1] == b[j - 1])
				table[i][j] = table[i - 1][j - 1] + 1;
			else
				table[i][j] = max(table[i - 1][j], table[i][j - 1]);
		}
	#ifdef DEBUG
		cout << endl << "==============" << endl << "DEBUG PRINT " << i << endl << "==============" << endl << endl;
		for (size_t k = 0; k <= a.size(); k++) {
			for (size_t l = 0; l <= b.size(); l++)
				cout << table[k][l] << " ";			
			cout << endl;
		}
		#endif
	}
	cout << table[a.size()][b.size()] << endl;
	system("pause");
	
}