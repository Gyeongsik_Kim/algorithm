#include <iostream>
#include <cinttypes>

#define DEBUG

typedef struct {
	char* str;
	uint64_t lenght;
} Item;

using namespace std;
uint64_t make_hash(Item item) {
	uint64_t hash = 0;
	for (auto i = 0; i < item.lenght; i++) {
		hash *= 26;
		hash += *(item.str + i);
		hash %= UINT64_MAX;
	}
	return hash;
}

/*
a -> 원본
b -> 찾을 데이터
*/
void match(Item a, Item b) {
	if (a.lenght < b.lenght) {
		cerr << "찾을 데이터 보다 원문이 더 짧습니다" << endl;
		return;
	}
	uint64_t hash = make_hash(b);
	Item item;
	item.lenght = b.lenght;
	for (uint64_t i = 0; i <= a.lenght - b.lenght; i++) {
		item.str = (a.str) + i;
		uint64_t ori_hash = make_hash(item);
#ifdef DEBUG
		cout << "=========" << i << "=========" << endl;
		cout << "원문 해시 : " << ori_hash << endl;
		cout << "비교 해시 : " << hash << endl;
#endif
		//FIND!
		if (hash == ori_hash)
			cout << "[FIND] INDEX : " << i << endl;
	}
}

void main() {
	char temp[UINT16_MAX];
	Item a, b;
	cout << "원문 : ";
	cin.clear();
	cin.getline(temp, UINT16_MAX);
	a.str = (char*)malloc(strlen(temp) * sizeof(char));
	strncpy(a.str, temp, strlen(temp) + 1);

	memset(temp, 0x00, UINT16_MAX);

	cout << "찾을 문자열 :";
	cin.clear();
	cin.getline(temp, UINT16_MAX);
	b.str = (char*)malloc(strlen(temp) * sizeof(char));
	strncpy(b.str, temp, strlen(temp) + 1);

	a.lenght = strlen(a.str);
	b.lenght = strlen(b.str);
	
	#ifdef DEBUG
		cout << "A : " << a.str << endl;
		cout << "A size : " << a.lenght << endl;
		cout << "B : " << b.str << endl;
		cout << "B size : " << b.lenght << endl;
	#endif

	match(a, b);
	system("pause");
}